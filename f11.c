#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <stdlib.h>
#include <stdio.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_native_dialog.h>
#include <locale.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

int main(){
    setlocale(LC_ALL, "Portuguese");
    int score = 0;
    int iy;
    int hitboxx = 0, hitboxy = 0;
    int ix = 1320;
    iy = rand() % 680;
    int tiro = 0;
    int yc = 240;
    int x, yb = yc, gb = 30;
    int xb = 0;
    char t[15];
    char v[10];

    if(!al_init()){
        al_show_native_message_box(NULL, NULL, "Error", "Could not initialize Allegro 5", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        return -1;
    }

    ALLEGRO_DISPLAY * janela;
    ALLEGRO_BITMAP *image = NULL;
    ALLEGRO_BITMAP *inicio = NULL;
    ALLEGRO_SAMPLE *sample = NULL;

    ALLEGRO_KEYBOARD_STATE teclado;
    ALLEGRO_MOUSE_STATE mouse;

    al_init();
    al_init_primitives_addon();
    al_install_keyboard();
    al_install_mouse();
    al_init_font_addon();
    al_init_ttf_addon();
    al_init_image_addon();
    al_install_audio();
    al_init_acodec_addon();
    al_reserve_samples(1);

    janela = al_create_display(1280, 720);
    al_set_window_title(janela, "F11KING TRIANGLE IN QUADRADOS ISLAND HD EDITION: O inimigo agora é o mesmo");
    al_hide_mouse_cursor(janela);

    ALLEGRO_FONT* fonte = al_load_font("Arial.ttf", 25, 0);
    image = al_load_bitmap("image.png");
    sample = al_load_sample( "music.wav" );
    inicio = al_load_bitmap("inicio.png");


    if(!image) {
      al_show_native_message_box(janela, "Error", "Error", "Failed to load image!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
      al_destroy_display(janela);
      return -1;
   }

    if(!fonte){
      printf("Não consegui carregar a fonte.\n");
      return -1;
    }
    int vida = 3;

    //al_set_new_display_flags(ALLEGRO_FULLSCREEN);

    // TELA DE INICIO
for(;;){

    al_get_keyboard_state(&teclado);
    al_get_mouse_state(&mouse);

    al_play_sample(sample, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_LOOP,NULL);

    al_clear_to_color(al_map_rgb(0,0,0));

    al_draw_bitmap(inicio,0,0,0);

    if(al_key_down(&teclado, ALLEGRO_KEY_ESCAPE)){
        al_show_native_message_box(janela, "Fim de Jogo", "Obrigado por jogar", "Volte sempre!", NULL, NULL);
        !al_hide_mouse_cursor(janela);
        return 0;
}

    al_draw_textf(fonte, al_map_rgb(255,255,255), 400, 60, 0, "F11KING TRIANGLE IN QUADRADOS ISLAND");

    al_draw_textf(fonte, al_map_rgb(255,255,255), 470, 680, 0, "Punch ENTER to START THIS!!!");

    al_draw_textf(fonte, al_map_rgb(0,0,255), 300, 410, 0, "Durante eras e eras, os triangulos tinham problemas com os quadrados");
    al_draw_textf(fonte, al_map_rgb(0,0,255), 300, 440, 0, "e voce, seu imbecil, voce se perdeu na area deles!!!");
    al_draw_textf(fonte, al_map_rgb(0,0,255), 300, 470, 0, "Agora atire e mate tudo que se move!!!(com o mouse apenas)");


    al_flip_display();

    // TELA DE INICIO

    if(al_key_down(&teclado, ALLEGRO_KEY_ENTER)){

    for(;;){


        al_get_keyboard_state(&teclado);
        al_get_mouse_state(&mouse);

        if(al_key_down(&teclado, ALLEGRO_KEY_ESCAPE)){
           al_show_native_message_box(janela, "Fim de Jogo", "Obrigado por jogar", "Volte sempre!", NULL, NULL);
           !al_hide_mouse_cursor(janela);
           return 0;
        }

        //Atirar e se movimentar
        yc = mouse.y;
        if(mouse.buttons == 1){
            xb = 10;
            yb = yc;
        }

        xb += 30; //Velocidade do tiro
        Sleep(1);
        ix -= 0.1; //Velocidade do inimigo

        if(ix <= 0){
            vida -= 1;
            ix = 1320;
            iy = rand() % 680;
        }

        if(vida < 0){
            al_show_native_message_box(janela, "GAME OVER", "", "VOCE PERDEU!!!!", NULL, NULL);
            !al_hide_mouse_cursor(janela);
            return 0;
        }

        //Caixa de Colisão
        for(hitboxx = 0;hitboxx <= 20; hitboxx++){
            if((xb == ix + hitboxx) || (xb == ix - hitboxx)){
                for(hitboxy = 0; hitboxy <= 20; hitboxy++){
                if((yb == iy + hitboxy) || (yb == iy - hitboxy)){
                    score += 1;
                    ix = 1320;
                    iy = rand() % 680;

            }

        }
    }
}
        //Aumento de dificuldade
        if(score >= 10){
            Sleep(0.1);
            ix -= 0.1;
        }else if(score >= 20){
            Sleep(0.1);
            ix -= 0.01;
        }else if(score >= 30){
            Sleep(0.1);
            ix -= 0.02;
        }else if(score >= 40){
            Sleep(0.1);
            ix -= 0.2;
        }else if(score >= 50){
            Sleep(0.1);
            ix -= 0.2;
        }else if(score >= 60){
            Sleep(0.1);
            ix -= 0.2;
        }

        sprintf(t,"Score: %d ", score);
        sprintf(v,"Vidas: %d ", vida);

        al_clear_to_color(al_map_rgb(0,0,0));

        al_draw_bitmap(image,0,0,0);

        al_draw_textf(fonte, al_map_rgb(255,255,255), 1150, 30, 0, t);
        al_draw_textf(fonte, al_map_rgb(255,255,255), 1150, 60, 0, v);

        al_draw_filled_triangle(0, yc - 10, 0, yc + 10, 30, yc, al_map_rgb(255,255,255));
        al_draw_filled_circle(xb, yb, 2, al_map_rgb(0,0,255));
        al_draw_filled_rectangle(ix, iy, ix + 20, iy +20, al_map_rgb(128,0,0));

        al_play_sample(sample, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_LOOP,NULL);


        al_flip_display();


    }
}
}
    return 0;
}